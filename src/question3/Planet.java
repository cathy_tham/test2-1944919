package question3;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet> {
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	/**
	 * Override the equals method
	 * equal if both their name and order fields are identical
	 * @param o Object 
	 * @return true or false boolean 
	 * @author Cathy Tham
	 */
	@Override
	public boolean equals(Object o) {
		
		if (!(o instanceof Planet)){
			return false;
		}

		Planet p = (Planet)o;
		
		if (this.name.equals(p.name) && this.order==p.order) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Override the hashCode()
	 * Uses name, order based on the fields of equals
	 * @return a combined String hashCode.
	 * @author Cathy Tham
	 */
	@Override
	public int hashCode() {
		String combined = this.order + this.name;
		return combined.hashCode();
	}
	
	/**
	 * Override the compareTo method from the Comparable<E> class
	 * sorted first by name (increasing order) and in the case that the names are equal, then by planetarySystemName (decreasing order)
	 * @param p Planet represent another planet used to compare
	 * @return positive number if this.name comes before other and negative is this.name comes after.
	 * @author Cathy Tham
	 *
	 */
	@Override
	public int compareTo(Planet p) {
		int nameCompare=this.name.compareTo(p.name);
		//If the name is the same, it will look at the planetarySystemName
		if( nameCompare == 0 ) {
			return  -1*(this.getPlanetarySystemName().compareTo(p.getPlanetarySystemName()));
		}
		//Else, it will only look at the name
		return nameCompare;
	}
}

