package question2;

public interface Employee {
	/**
	 *Method that all classes that implements Employee need to Override 
	 */
	double getYearlyPay();

}