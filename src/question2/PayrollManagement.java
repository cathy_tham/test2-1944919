package question2;

//PayrollManagement is the main method to print the yearly payment of an employee
public class PayrollManagement {

	public static void main(String[] args) {
		Employee[] empArr = new Employee[5];
		empArr[0] = new HourlyEmployee(5,13.0);
		empArr[1] = new UnionizedHourlyEmployee(40,10.0,5000.0);
		empArr[2] = new HourlyEmployee(19,30.0);
		empArr[3] = new SalariedEmployee(200000.0);
		empArr[4] = new SalariedEmployee(50000.0);
		
		double totalYearlyExpenses = getTotalExpenses(empArr);
		System.out.println("The total expenses is: " +totalYearlyExpenses + "$.");
	}
	/**
	 * getTotalExpenses() add up each employee's yearly pay 
	 * @param empArr an Employee[] 
	 * @return totalYearlyExpenses
	 */
	public static double getTotalExpenses(Employee[] empArr) {
		double totalYearlyExpenses = 0.0;
		for (Employee emp : empArr) {
			totalYearlyExpenses +=emp.getYearlyPay();
		}
		return totalYearlyExpenses;
	}

}