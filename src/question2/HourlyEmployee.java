package question2;

public class HourlyEmployee implements Employee {
	private double hoursPerWeek;
	private double hourlyPay;
	
	/**
	 * Constructor
	 * @param hoursPerWeek a double represent the worked hours per week
	 * @param hourlyPay a double represent the pay per hour
	 */
	public HourlyEmployee(double hoursPerWeek, double hourlyPay) {
		this.hoursPerWeek=hoursPerWeek;
		this.hourlyPay=hourlyPay;
	}
	
	/**
	 * GetterhoursPerWeek
	 * @return hoursPerWeek 
	 */
	public double getHoursPerWeek() {
		return this.hoursPerWeek;
	}
	
	/**
	 * Getter for hourlyPay
	 * @return hourlyPay
	 */
	public double getHourlyPay() {
		return this.hourlyPay;
	}

	
	/**
	 * Overriding the getYearlyPay()
	 * GetYearlyPay() multiplies the number of hours worked per week by the pay rate per hour multiplied by 52 
	 *@return the yearly salary
	 */
	@Override
	public double getYearlyPay() {
		final int numberWeeksInYear=52;
		return hoursPerWeek*hourlyPay*numberWeeksInYear;
		
		
	}
	

}
