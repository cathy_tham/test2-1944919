package question2;

public class SalariedEmployee implements Employee {
	private double yearlySalary;
	
	/**
	 * Constructor
	 * @param hoursPerWeek a double represent the yearlySalary
	 */
	public SalariedEmployee(double yearlySalary) {
		this.yearlySalary = yearlySalary;
	}

	
	/**
	 * Overriding the getYearlyPay()
	 * getYearlyPay() returns the private field
	 *@return the yearly salary
	 */
	@Override
	public double getYearlyPay() {
		return yearlySalary;
		
	}

}