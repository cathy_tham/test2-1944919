package question2;

public class UnionizedHourlyEmployee extends HourlyEmployee{
	private double pensionContribution;
	
	/**
	 * Constructor
	 * @param hoursPerWeek a double represent the worked hours per week
	 * @param hourlyPay a double represent the pay per hour
	 * @param pensionContribution a double
	 */
	public UnionizedHourlyEmployee(double hoursPerWeek, double hourlyPay,double pensionContribution) {
		super(hoursPerWeek, hourlyPay);
		this.pensionContribution=pensionContribution;
	}
	
	/**
	 * Overriding the getYearlyPay()
	 * GetYearlyPay() return the same as an HourlyEmployee except that there is an additional pensionContribution added to it
	 *@return the yearly salary
	 */
	@Override
	public double getYearlyPay() {
		return super.getYearlyPay()+ pensionContribution;
		
	}
}
