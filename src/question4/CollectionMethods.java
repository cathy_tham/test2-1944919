package question4;

import java.util.ArrayList;
import java.util.Collection;

import question3.Planet;


public class CollectionMethods {
	
	/**
	 * The method examines the planets and return only those planets which are of the first 3 planets in their system 
	 * @param planets
	 * @return Collection with the first 3 planets in their system
	 */
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
		Collection<Planet> innerPlanets = new ArrayList<Planet>();
		//Loops through all the planets
		for (Planet p : planets) {
			//If the planet are the first 3 planets in their system
			if(p.getOrder() == 1 || p.getOrder() == 2 || p.getOrder() == 3 ) {
				innerPlanets.add(p);
			}
		}
		return innerPlanets;
	}

}